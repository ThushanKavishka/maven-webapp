/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.entgra.web.dao.impl;

import com.entgra.web.dao.DataServiceDao;
import com.entgra.web.dao.Food;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

/**
 * This class represents an implementation of DataServiceDao.
 */
public class DataServiceDaoImpl implements DataServiceDao {
    private final static Logger logger = Logger.getLogger(DataServiceDaoImpl.class);
    // JDBC Driver Name & Database URL
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static final String JDBC_DB_URL = "jdbc:mysql://localhost:3306/food";

    // JDBC Database Credentials
    private static final String JDBC_USER = "root";
    private static final String JDBC_PASS = "12345";

    private static GenericObjectPool gPool = null;
    private Connection connection = null;
    private PreparedStatement preparedStatement = null;

    @Override
    public boolean insertXmlPayloadData(Food food) {
        try {
            boolean isConnected = getDbConnection();
            if (isConnected) {
                logger.debug("DB Connection Successful.");
                // PreparedStatements can use variables and are more efficient
                preparedStatement = connection
                        .prepareStatement("insert into  food.breakfast values (default, ?, ?, ?, ?)");

                // Parameters start with 1
                preparedStatement.setString(1, food.getName());
                preparedStatement.setBigDecimal(2, food.getPrice());
                preparedStatement.setString(3, food.getDescription());
                preparedStatement.setInt(4, food.getCalories());
                preparedStatement.executeUpdate();
            } else {
                logger.debug("DB Connection Failed.");
                return false;
            }
        } catch (SQLException | NumberFormatException e) {
            logger.fatal(e.getMessage());
            return false;
        } finally {
            closeDbConnection();
        }
        return true;
    }

    /**
     * setup new jdbc connection
     */
    private boolean getDbConnection() {
        try {
            DataSource dataSource = setUpPool();
            // Setup the connection with the DB
            connection = dataSource.getConnection();
            printDbStatus();
            return true;
        } catch (Exception e) {
            logger.fatal(e.getMessage());
            return false;
        }
    }

    /**
     * close the connection
     */
    private void closeDbConnection() {
        try {
            if (connection != null) {
                connection.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
        } catch (SQLException e) {
            logger.fatal(e.getMessage());
        }
    }

    /**
     * create connection pool
     */
    @SuppressWarnings("unused")
    private DataSource setUpPool() throws Exception {
        Class.forName(JDBC_DRIVER);

        // Creates an Instance of GenericObjectPool That Holds Our Pool of Connections Object
        gPool = new GenericObjectPool();
        gPool.setMaxActive(5);

        // Creates a ConnectionFactory Object Which Will Be Use by the Pool to Create the Connection Object
        ConnectionFactory cf = new DriverManagerConnectionFactory(JDBC_DB_URL, JDBC_USER, JDBC_PASS);

        // Creates a PoolableConnectionFactory That Will Wraps the Connection Object Created by the ConnectionFactory to Add Object Pooling Functionality
        PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, gPool, null, null, false, true);
        return new PoolingDataSource(gPool);
    }

    private GenericObjectPool getConnectionPool() {
        return gPool;
    }

    /**
     * print the connection pool status
     */
    private void printDbStatus() {
        logger.info("Max.: " + getConnectionPool().getMaxActive() + "; Active: " + getConnectionPool().getNumActive() + "; Idle: " + getConnectionPool().getNumIdle());
    }

}
