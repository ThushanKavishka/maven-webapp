/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.entgra.web.api;

import com.entgra.web.service.FoodService;
import org.apache.log4j.Logger;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * This class represents an implementation of RESTController.
 */
@Path("/food")
public class RESTControllerImpl implements RESTController {
    private final static Logger logger = Logger.getLogger(RESTControllerImpl.class);
    private FoodService foodService = new FoodService();

    @Override
    public Response validateXmlPayload(String xmlPayload) {
        logger.debug(xmlPayload);
        boolean isValid = foodService.validateFoodXml(xmlPayload);
        if (!isValid) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        } else {
            return Response.ok().build();
        }
    }

    @Override
    public Response convertXmlPayloadToJSON(String xmlPayload) {
        String JSONPayload = "";
        logger.debug(xmlPayload);
        boolean isValid = foodService.validateFoodXml(xmlPayload);
        if (!isValid) {
            return Response.status(Response.Status.NOT_ACCEPTABLE).build();
        } else {
            JSONPayload = foodService.convertFoodXmlToJSON(xmlPayload);
        }
        return Response.ok().entity(JSONPayload).build();
    }

    @Override
    public Response insertXmlPayloadData(String xmlPayload) {
        logger.debug(xmlPayload);
        boolean isUpdate = foodService.insertXmlPayloadData(xmlPayload);
        if (isUpdate) {
            logger.debug("Database updated successfully.");
            return Response.ok().build();
        } else {
            logger.debug("Error updating database.");
            return Response.notModified().build();
        }
    }
}
