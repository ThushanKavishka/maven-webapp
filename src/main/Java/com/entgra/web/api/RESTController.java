/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.entgra.web.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * This interface is the representation of endpoints
 */
public interface RESTController {

    /**
     * validate xml payload
     *
     * @param xmlPayload
     * @return
     */
    @POST
    @Path("/validate")
    @Consumes(MediaType.APPLICATION_XML)
    Response validateXmlPayload(String xmlPayload);

    /**
     * convert xml payload into json format
     *
     * @param xmlPayload
     * @return
     */
    @POST
    @Path("/convert")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_JSON)
    Response convertXmlPayloadToJSON(String xmlPayload);

    /**
     * insert xml payload data into database
     *
     * @param xmlPayload
     * @return
     */
    @POST
    @Path("/jdbc")
    @Consumes(MediaType.APPLICATION_XML)
    Response insertXmlPayloadData(String xmlPayload);

}
