/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.entgra.web.service;

import com.entgra.web.dao.Food;
import com.entgra.web.dao.DataServiceDao;
import com.entgra.web.dao.impl.DataServiceDaoImpl;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.StringReader;

/**
 * This class represents the all the services which map with endpoints.
 */
public class FoodService {
    private final static Logger logger = Logger.getLogger(FoodService.class);
    private DataServiceDao dataServiceDao = new DataServiceDaoImpl();

    /**
     * validate xml payload
     *
     * @param xmlPayload xml string
     * @return boolean true of false
     */
    public boolean validateFoodXml(String xmlPayload) {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(xmlPayload));
            Document doc = builder.parse(src);
            String context = doc.getElementsByTagName("context").item(0).getTextContent();

            Schema schema = schemaFactory.newSchema(new File(context));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new StringReader(xmlPayload)));
            return true;
        } catch (Exception e) {
            logger.fatal(e.getMessage());
            return false;
        }
    }

    /**
     * convert xml payload into json format
     *
     * @param xmlPayload xml string
     * @return json payload
     */
    public String convertFoodXmlToJSON(String xmlPayload) {
        try {
            JSONObject xmlJSONObj = XML.toJSONObject(xmlPayload);
            int PRETTY_PRINT_INDENT_FACTOR = 4;
            return xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
        } catch (JSONException e) {
            logger.fatal(e.getMessage());
            return null;
        }
    }

    /**
     * insert xml payload data into database
     *
     * @param xmlPayload  xml string
     * @return boolean true of false
     */
    public boolean insertXmlPayloadData(String xmlPayload) {
        try {
            Food food = mapXmlPayload(xmlPayload);
            if (food != null) {
                dataServiceDao.insertXmlPayloadData(food);
            } else {
                logger.debug("Invalid food data.");
                return false;
            }
            return true;
        } catch (JSONException e) {
            logger.fatal(e.getMessage());
            return false;
        }
    }

    /**
     * map xml payload with pojo
     *
     * @param xmlPayload xml string
     * @return
     */
    private Food mapXmlPayload(String xmlPayload) {
        try {
            StringReader sr = new StringReader(xmlPayload);
            JAXBContext jaxbContext = JAXBContext.newInstance(Food.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            Food food = (Food) unmarshaller.unmarshal(sr);
            logger.debug(food.getName());
            return food;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
